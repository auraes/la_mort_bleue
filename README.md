**La Mort Bleue**

Copyright (C) Auraes, 20014-2022, pour le code.  
Licence GPL-2.0-or-later

Copyright (C) Eric Forgeot pour le texte.  
Licence libre CC BY-SA 3.0  
https://github.com/farvardin/textallion/tree/master/samples_cyoa

Adaptation du jeu en Z-code, avec le compilateur Inform6, et en langage C – GCC (UTF-8) et Turbo C (DOS).  
https://gitlab.com/auraes/la_mort_bleue

La Mort bleue est un jeu narratif à choix multiples ; le déroulement de l’histoire dépend des choix du joueur.  
Il a été réalisé pour accompagner le module de création de livres-jeux de l’outil Textallion ; logiciel qui permet d’éditer et de publier de la littérature en prose et de la poésie, vers les formats html, pdf ou epub.