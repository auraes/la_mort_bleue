/* Copyright (c) 2020 Auraes, pour le code en langage c
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Ce programme est un logiciel libre ; vous pouvez le redistribuer ou le 
 * modifier suivant les termes de la GNU General Public License telle que 
 * publiée par la Free Software Foundation ; soit la version 2 de la licence, 
 * soit (à votre gré) toute version ultérieure.
 * Ce programme est distribué dans l'espoir qu'il sera utile, mais SANS AUCUNE 
 * GARANTIE ; sans même la garantie tacite de QUALITÉ MARCHANDE ou d'ADÉQUATION 
 * à UN BUT PARTICULIER. Consultez la GNU General Public License pour plus de 
 * détails. Vous devez avoir reçu une copie de la GNU General Public License
 * en même temps que ce programme ; si ce n'est pas le cas, 
 * consultez <http://www.gnu.org/licenses>.

 * La Mort Bleue, copyright (c) Eric Forgeot pour le texte
 * LICENCE LIBRE : CC-BY-SA https://creativecommons.org/licenses/by-sa/3.0/deed.fr
*/

/* gcc -Wall -W -pedantic -std=c99 cyoa_utf8.c -ocyoa */

/* Source encoding UTF-8 No Mark */

#include <wchar.h>
#include <wctype.h>
#include <locale.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define SAISIEMAX 7

typedef void (*page)(void);

wchar_t buffer[SAISIEMAX+1];
int dice[] = { 1, 2, 3, 4, 5, 6 };

void msg(const wchar_t *str);
void readline(wchar_t *buf, size_t size);
int choix(const wchar_t *str);
int str_len(const wchar_t *pt);
void new_line(void);
page chance(page lucky, page unlucky);

int flag_readline;
int xc;
int cwidth;

#include "pages_utf8.c"

page chance(page lucky, page unlucky)
{
   int n, num, pt;

   n = 6;
   while(n) {
      num = rand() % n; /* 0-5 */
      n--;
      pt = dice[num];
      dice[num] = dice[n];
      dice[n] = pt;
   }
   msg(L"Tentez votre Chance, choisissez une des six faces du dé !");

   readline(buffer, SAISIEMAX+1);

   if (*buffer >= L'1' && *buffer <= L'6') {
     if (dice[*buffer - L'0'] >= 5) {
         msg(L" Je suis chanceux.");
         putwchar(L'\n');
         return ((page)lucky);
     }
   }
   msg(L" Je suis malchanceux.");
   putwchar(L'\n');
   return ((page)unlucky);
}

void new_line(void)
{
   xc = 1;
   putwchar(L'\n');
   putwchar(L' ');
}

int str_len(const wchar_t *pt)
{
   const wchar_t *p = pt;
   if (*pt == L' ' || *pt == L'\n') return -1;
   while (*pt != L'\0' && *pt != L' ' && *pt != L'\n') pt++;
   return (pt-p);
}

void msg(const wchar_t *str)
{
   int i;

   while(1) {   
      i = str_len(str);
      if (i == 0) {
         new_line();
         break;
      }
      if (i == -1) {
         if (*str == L'\n') new_line();
         else {
            if (xc > cwidth) {
               new_line();
               if (*str == L' ') goto b0;
            }
            putwchar(*str);
            if (xc > (cwidth-1)) new_line();
            else xc++;
         }
         b0:
         str++;
         continue;
      }
      if ((xc+i) > (cwidth+1)) new_line();
      while (i) {
         putwchar(*str++);
         xc++;
         i--;
      }
   }
}

int choix(const wchar_t* str)
{
   if (flag_readline == 0) {
      flag_readline = 1;
      readline(buffer, SAISIEMAX+1);
   }
   return !(wcscmp(str, buffer));
}

void readline(wchar_t *buf, size_t size)
{
   wint_t c;

   putwchar(L'>');
   if (fgetws(buf, size, stdin) == NULL) return; /* TODO */
   putwchar(L'\n');
   while (*buf != L'\0') {
      if (*buf == L'\n') {
         *buf = L'\0';
         return;
      }
      *buf = towlower(*buf);
      buf++;	
   }	
   do {
      c = fgetwc(stdin);
   } while (c != L'\n' && c != WEOF); /* WEOF TODO */
}

int main(int nbarg, char *argv[])
{	
   page (*pf)(void);
   page (*pt)(void);

   setlocale(LC_ALL, ""); 
   system("clear");
   srand (time(NULL));

   cwidth = 66; 
   if (nbarg > 1) {
      cwidth = atoi(argv[1]);
      if (cwidth == 0 || cwidth < 40 || cwidth > 80) cwidth = 66;
   }

   putwchar(L'\n');
   pf = p0;
   while(1) {
      flag_readline = 0;
      pt = (page (*)()) pf();
      if (pt == NULL) {
         if (flag_readline == 0) { /* WARNING 1er */
            msg(L"\nFin.\n");
            break;
         }
         if (choix(L"fin")) break;
      }
      else pf = pt;
   }
   return EXIT_SUCCESS;
}

