/*
 * Fichier associé et inclus par cyoa_utf8.c 
 * Source encoding UTF-8 No Mark
*/
page p0(void);
page p1(void);
page p2(void);
page p3(void);
page p4(void);
page p5(void);
page p6(void);
page p7(void);
page p8(void);
page p9(void);
page p10(void);
page p11(void);
page p12(void);
page p13(void);
page p14(void);
page p15(void);
page p16(void);
page p17(void);
page p18(void);
page p19(void);
page p20(void);
page p21(void);
page p22(void);
page p23(void);
page p24(void);
page p25(void);
page p26(void);
page p27(void);
page p28(void);
page p29(void);
page p30(void);
page p31(void);
page p32(void);
page p33(void);
page p34(void);
page p35(void);
page p36(void);
page p37(void);
page p38(void);
page p39(void);
page p40(void);
page p41(void);
page p42(void);
page p43(void);
page p44(void);
page p45(void);
page p46(void);
page p47(void);
page p48(void);
page p49(void);
page p50(void);
page p51(void);
page p52(void);
page p53(void);
page p54(void);
page p55(void);

page p0(void)
{
   msg(L" La mort bleue\nUne fiction interactive d’Otto Grimwald.\n\n"
      "Pour progresser dans le jeu, choisissez parmi les différentes propositions celle qui vous convient, tapez le chiffre qui la précède et validez-le en appuyant sur la touche ’Entrée’. Vous pouvez quitter la partie en cours en tapant ’fin’.\n"
   );

   msg(L"1. Et maintenant, tournez la page !");

   if (choix(L"1")) return (page)p1;
   return NULL;
}

page p1(void) 
{
   msg(L" == 1 ==\n"
      "Je me sens tomber. Pas très haut par rapport au sol, heureusement pour moi. Et quelques cordages épais amortissent ma chute. J’ai probablement encore raté un échelon, car je suis dans la lune la plupart du temps en ce moment.\n"
   );

   msg(L"1. Je me lève");

   if (choix(L"1")) return (page)p19;
   return NULL;
}

page p2(void) 
{
   msg(L" == 2 ==\n"
      "Dès que je pose le pied sur le seuil de ce grand bâtiment, des officiers de police m’arrêtent, au motif que je me suis évadé de la maison où je devais rester, ayant été acquit légalement par M. et Mme Caesar.\n"
   );

   msg(L"1. En dépit de mes protestations, je suis jeté en prison");

   if (choix(L"1")) return (page)p48;
   return NULL;
}

page p3(void) 
{
   msg(L" == 3 ==\n"
      "Il y a là un groupe de six marins. Ils ne sont qu’en train de s’amuser après tout !\n"
   );

   msg(L""
      "1. Mais je peux leur donner une correction méritée\n"
      "2. ou bien m’enfuir lâchement sur le pont supérieur"
   );

   if (choix(L"1")) return (page)p23;
   if (choix(L"2")) return (page)p5;
   return NULL;
}

page p4(void) 
{
   msg(L" == 4 ==\n"
      "Je sonne à la porte. Béatrix m’ouvre, et me voyant, ne semble pas très heureuse de ma présence. Nous étions bons amis auparavant, aussi je ne comprends pas ses réserves.\n\n"
      "Elle porte une jolie petite robe légère, blanche et grise, et je ressens tant de poésie dans l’atmosphère que je ne peux m’empêcher de lui dire ce que j’ai sur le cœur.\n"
   );

   msg(L""
      "1. Être grossier, mais direct\n"
      "2. Être modéré, mais verbeux"
   );

   if (choix(L"1")) return (page)p35;
   if (choix(L"2")) return (page)p28;
   return NULL;
}

page p5(void) 
{
   msg(L" == 5 ==\n"
      "Ce pont est habituellement réservé aux passagers de première classe. J’espère qu’aucun officier du navire ne me remarquera, sinon j’aurai de sérieux problèmes.\n"
      "J’obtins ce travail sur un navire pour payer ma traversée vers le Grand Continent, qui a été découvert il y a seulement quelques siècles.\n"
      "En attendant, j’ai toujours été curieux au sujet de la vie là-bas, de savoir si les gens étaient comme nous ou si leur culture était si étrangère qu’il n’était pas possible d’y vivre.\n\n"
      "J’ai également essayé d’apprendre leur si difficile langue, et je me suis forcé à l’utiliser quotidiennement. C’est ainsi que j’ai déménagé vers Iricimia, qui a une langue proche, et même si je suis loin de complètement la maîtriser, j’ai vécu là-bas un moment, puis j’ai décidé d’embarquer sur ce bateau.\n"
   );

   msg(L""
      "1. Après quelques jours, nous arrivons à New Londrin Haven, et j’entre triomphalement dans la ville\n"
      "2. mais avant cela, je peux explorer un moment le port, amoindrissant par contre le prestige de mon arrivée"
   );

   if (choix(L"1")) return (page)p25;
   if (choix(L"2")) return (page)p27;
   return NULL;
}

page p6(void) 
{
   msg(L" == 6 ==\n"
      "Nous entrons dans un salon de thé à l’ambiance snobinarde, dans une zone touristique. Nous nous ennuyons rapidement en un tel lieu, aussi je propose que nous cherchions un endroit plus agréable pour nous divertir.\n"
   );

   msg(L""
      "1. Proposer son appartement\n"
      "2. Proposer mon appartement\n"
      "3. Proposer de prendre un verre dans une taverne proche"
   );

   if (choix(L"1")) return (page)p42;
   if (choix(L"2")) return (page)p38;
   if (choix(L"3")) return (page)p7;
   return NULL;
}

page p7(void) 
{
   msg(L" == 7 ==\n"
      "Entrant dans la taverne, je retrouve certains de mes anciens camarades du bateau. Étrangement, ils ne sont pas trop hostiles à ma présence. Nous prenons une bière, puis une autre, et finalement je termine la nuit à danser sur les tables, pour la plus grande joie des femmes ici qui frappent dans leurs délicieuses mains en rythme.\n\n"
      "J’en viens à apprécier cette vie simple, et je ne crains plus cette terreur bleue que j’avais l’habitude de ressentir dans le passé."
   );

/* Fin */
   return NULL;
}

page p8(void) 
{
   msg(L" == 8 ==\n"
      "Ai-je de la chance aujourd’hui ? Quand je tire un dé de ma poche, je peux être considéré comme chanceux si j’obtiens un 5 ou un 6.\n"
   );

   return chance((page)p18, (page)p51);
}

page p9(void) 
{
   msg(L" == 9 ==\n"
      "L’opposition est remplie de personnes charmantes et engagées, néanmoins je ne peux garantir que leurs dirigeants soient aussi propres politiquement. Il y en a même qui racontent que la tête pensante de l’opposition est infiltrée par l’Indigo Love Syndicate.\n"
   );

   return chance((page)p32, (page)p49);
}

page p10(void) 
{
   msg(L" == 10 ==\n"
      "Les quais ne sont pas un endroit très agréable pour un être aussi délicat que moi. J’ai immédiatement le sentiment que je pourrais devenir une proie facile pour la plupart des vautours qui sévissent ici.\n\n"
       "Je remarque une belle femme qui attend près d’un ponton. Au même moment, des marins, rencontrés sur le bateau où j’étais employé, m’appelle tandis qu’ils entrent dans une taverne.\n"
   );

   msg(L""
      "1. M’approcher de cette dame\n"
      "2. Suivre mes anciens camarades dans la taverne\n"
      "3. Retourner chez moi"
   );

   if (choix(L"1")) return (page)p37;
   if (choix(L"2")) return (page)p7;
   if (choix(L"3")) return (page)p31;
   return NULL;
}

page p11(void) 
{
   msg(L" == 11 ==\n"
      "Impassible aux émotions humaines, le navire atteint les premières brumes matinales de l’autre continent. Foulant la jetée et la terre ferme pour la première fois après trois semaines de torture spirituelle, je me sens étourdi et complètement perdu.\n\n"
      "Il y a un grand poster sur un mur, et la sortie vers le reste de la ville.\n"
   );

   msg(L""
      "1. Regarder le poster\n"
      "2. Sortir"
   );

   if (choix(L"1")) return (page)p27;
   if (choix(L"2")) return (page)p25;
   return NULL;
}

page p12(void) 
{
   msg(L" == 12 ==\n"
      "Avant de commettre l’irréparable, je remarque l’ombre de Béatrix arrivant derrière moi, bouleversée et angoissée à mon sujet.\n"
   );

   msg(L"1. Elle me dit qu’elle regrette sa réaction trop vive, et propose que l’on prenne un verre ensemble dans un salon de thé dans les environs");

   if (choix(L"1")) return (page)p6;
   return NULL;
}

page p13(void) 
{
   msg(L" == 13 ==\n"
      "Le capitaine, arrivant peu après, s’excuse pour la conduire grossière de ses hommes. S’inclinant profondément, il me demande mon opinion sur la route à suivre. Est-ce que je rêve ? il est devenu fou, ou bien c’est une sorte de pitié malsaine à mon encontre…\n"
   );

   msg(L""
      "1. Au nord, toujours vers notre destination initialement prévue, le Nouveau Continent\n"
      "2. Au sud, pour atteindre une petite île qu’il a juste découverte avec sa longue-vue\n"
      "3. À l’ouest, pour explorer encore plus ce vaste océan bleu\n"
      "4. À l’est, pour retourner vers notre pays d’origine"
   );

   if (choix(L"1")) return (page)p11;
   if (choix(L"2")) return (page)p30;
   if (choix(L"3")) return (page)p43;
   if (choix(L"4")) return (page)p24;
   return NULL;
}

page p14(void) 
{
   msg(L" == 14 ==\n"
      "1. Que la mort peut être douce, lorsqu’elle est si bleue !"
   );

/* Fin */
   return NULL;
}

page p15(void) 
{
   msg(L" == 15 ==\n"
      "Le soleil est haut dans le ciel, et pourtant je ressens une brise fraîche dévorant mon cou et mes côtes. Je me souviens de mon enfance en Francimia, mes jeux dans les arbres et entre les rares collines vertes de notre domaine familial. Plus tard, toujours dans les arbres, en train de lire des livres à thème fantastique, allongé sur le tapis rouge et violet dans ma cabane à huit mètres du sol. Je me rappelle également de ma mère m’appelant, avec sa voix haut perchée, pour venir manger la « Gargouaillotte à la Pisaille », un plat local.\n\n"
      "J’ai soudain l’impression de tourner en rond…\n"
   );

   msg(L""
      "1. Continuer à explorer\n"
      "2. Retourner au bateau"
   );

   if (choix(L"1")) return (page)p21;
   if (choix(L"2")) return (page)p11;
   return NULL;
}

page p16(void) 
{
   msg(L" == 16 ==\n"
      "Je n’aurais jamais imaginé qu’être vendu comme esclave puisse se passer ainsi. Ce n’était certes pas le marché aux esclaves sur la place publique comme on aurait pu l’imaginer, j’ai juste été conduit dans une cave sombre et humide, où je me suis ennuyé à mourir.\n"
   );

   msg(L"1. Quelques jours plus tard, j’ai été vendu à une famille aisée");

   if (choix(L"1")) return (page)p45;
   return NULL;
}

page p17(void) 
{
   msg(L" == 17 ==\n"
      "Je plonge dans l’eau sombre. Je me sens si seul, que rapidement j’arrête de respirer.\n"
   );

   msg(L"1. Je coule plus profondément");

   if (choix(L"1")) return (page)p14;
   return NULL;
}

page p18(void) 
{
   msg(L" == 18 ==\n"
      "Les murs sont maintenant plus propres sans ce poster malsain.\n"
   );

   msg(L"1. Je peux continuer mon chemin");

   if (choix(L"1")) return (page)p25;
   return NULL;
}

page p19(void) 
{
   msg(L" == 19 ==\n"
      "Je me suis senti déprimé tant d’années que je ne puis le supporter plus longtemps. De plus, mon lieu de résidence et mon emploi actuel menacent graduellement tant ma santé mentale que physique…\n\n"
      "L’océan, immense et bleu, s’étendant à l’infini vers le ’nord’, l’est’, le ’sud’ et l’ouest’, reflète le soleil comme une cotte de maille scintillante.\n\n"
      "« Hé ! Espèce de saloperie de Francimi. Viens par ici ! », crient-ils.\n\n"
      "Entouré de ces étrangers Scottani et Iricimi, je ne me sens pas très bien dans cet environnement. Leurs casques verts et orange foncés me rendent nerveux. Je n’ai pas besoin de leur obéir, car ce sont de simples marins comme moi. Mais vais-je tolérer ces insultes ?\n"
   );

   msg(L""
      "1. Me rebeller, rendez-vous au 3\n"
      "2. Attendre (rendez-vous au 13)\n"
      "3. Sauter dans la mer\n"
      "4. ou explorer un peu le bateau, ce qui est un prétexte pour fuir lâchement vers le 5"
   );

   if (choix(L"1")) return (page)p3;
   if (choix(L"2")) return (page)p13;
   if (choix(L"3")) return (page)p17;
   if (choix(L"4")) return (page)p5;
   if (choix(L"nord"))  return (page)p11;
   if (choix(L"sud"))   return (page)p30;
   if (choix(L"est"))   return (page)p24;
   if (choix(L"ouest")) return (page)p43;
   return NULL;
}

page p20(void) 
{
   msg(L" == 20 ==\n"
      "On me dit avec un ton irrité que je fais erreur. Ce n’est plus là depuis un moment.\n"
   );

   msg(L"1. On me donne un autre numéro de bureau, au cinquième étage, ce qui me conduit au 53");

   if (choix(L"1")) return (page)p53;
   return NULL;
}

page p21(void) 
{
   msg(L" == 21 ==\n"
      "À l’âge de dix-neuf ans, je suis allé dans la ville la plus proche, pour apprendre à devenir bibliothécaire. Mon père en fut un peu déçu, lui qui souhaitait que je devienne artiste. J’ai quand même emporté mon accordéon avec moi, pour lui faire plaisir.\n\n"
      "Satanés moustiques ! Avec toutes ces pestes volantes, il est bien difficile de se concentrer.\n"
   );

   msg(L""
      "1. Explorer un peu plus\n"
      "2. Retourner au bateau"
   );

   if (choix(L"1")) return (page)p15;
   if (choix(L"2")) return (page)p11;
   return NULL;
}

page p22(void) 
{
   msg(L" == 22 ==\n"
      "Cette ville est énorme selon les standards du Vieux Continent. De gigantesques dirigeables traversent pompeusement les airs, mais il y a également quelques avions, plus qu’en Euralinia.\n\n"
      "Les maisons ici sont plus opulentes, avec des influences marquées des villes d’Asinalia les plus extrêmes-orientales, dénotant un goût certain pour les décorations délicates et compliquées. Je n’aime pas cela du tout.\n\n"
      "Que puis-je faire maintenant ?\n"
   );

   msg(L""
      "1. Rencontrer les autorités locales, pour m’annoncer et m’enregistrer dans leurs services\n"
      "2. En apprendre plus sur leurs coutumes et traditions"
   );

   if (choix(L"1")) return (page)p44;
   if (choix(L"2")) return (page)p33;
   return NULL;
}

page p23(void) 
{
   msg(L" == 23 ==\n"
      "Des marins entraînés et en nombre sont certainement plus costauds qu’un pauvre vagabond comme moi qui a toujours vécu sur le plancher des vaches, entre la bibliothèque du coin et l’université.\n"
   );

   msg(L"1. Ils me jettent par-dessus bord");

   if (choix(L"1")) return (page)p17;
   return NULL;
}

page p24(void) 
{
   msg(L" == 24 ==\n"
      "Le navire n’avait probablement pas assez de vivres pour effectuer la traversée, et cela n’était pas une surprise de devoir faire demi-tour pour retourner dans notre pays d’origine.\n\n"
      "Déçus et fatigués, les marins quittent le bateau en colonnes disciplinées. Habitués aux petites traversées locales, ils ne sont pas près de retourner vers ce Nouveau Continent dont tout le monde parle.\n\n"
      "Je suis malgré tout heureux de revenir ici, et d’être débarrassé d’eux.\n"
   );

   msg(L""
      "1. Recommencer le voyage depuis le début\n"
      "2. Visiter les docks\n"
      "3. Retourner chez moi"
   );

   if (choix(L"1")) return (page)p1;
   if (choix(L"2")) return (page)p10;
   if (choix(L"3")) return (page)p31;
   return NULL;
}

page p25(void) 
{
   msg(L" == 25 ==\n"
      "Cette ville est énorme selon les standards du Vieux Continent. De gigantesques dirigeables traversent pompeusement les airs, mais il y a également quelques avions, plus qu’en Euralinia.\n\n"
      "Les maisons ici sont plus opulentes, avec des influences marquées des villes d’Asinalia les plus extrêmes-orientales, dénotant un goût certain pour les décorations délicates et ayant du caractère.\n\n"
      "Une telle sollicitude pour des motifs aussi inspirés m’inspire un tel bonheur que j’en oublie un moment ma solitude spirituelle.\n\n"
      "Le soleil commence à annihiler l’humidité poisseuse de la brume, en envoyant des rayons orange sur les murs.\n\n"
      "Que faire maintenant ?\n"
   );

   msg(L""
      "1. Rencontrer les autorités locales, pour m’annoncer et m’enregistrer dans leurs services\n"
      "2. En apprendre plus sur leurs coutumes et traditions\n"
      "3. Voler un peu de nourriture, personne ne me connaît ici après tout"
   );

   if (choix(L"1")) return (page)p44;
   if (choix(L"2")) return (page)p33;
   if (choix(L"3")) return (page)p40;
   return NULL;
}

page p26(void) 
{
   msg(L" == 26 ==\n"
      "« Cher voyageur,\n"
      "merci de participer à ce jeu. J’espère que vous ne vous sentirez pas trop limité ici, et que votre liberté de pensée n’est pas trop mise à mal. Cette courte histoire risque par contre de réellement commencer quand vous retournerez à la civilisation. N’espérez pas découvrir beaucoup de choses dans ces îles désertées. Au fait, si par hasard vous rencontrez Jacqueline, dites-lui bonjour de notre part ! »\n"
   );

   msg(L"1. L’esprit en paix et oubliant la tromperie verdoyante de la montagne, je manie frénétiquement les rames pour retourner au bateau");

   if (choix(L"1")) return (page)p11;
   return NULL;
}

page p27(void) 
{
   msg(L" == 27 ==\n"
      "Ce qui était le plus visible et étrange dans cet endroit, c’était un poster imprimé sur un fond bleu indigo, avec écrit par-dessus, en lettres énormes et roses :\n\n"
      "« REJOIGNEZ L’ARMÉE DE L’AMOUR™ !\n"
      "L’Indigo Love Syndicate™ a besoin de vous. Merci pour votre attention. »\n\n"
      "Il n’y a pas de moyen clair pour les contacter. Peut-être qu’ils sont devenus si puissants récemment qu’il est aisé de tomber sur eux. On verra bien !\n"
   );

   msg(L""
      "1. Déchirer ce poster\n"
      "2. S’agenouiller et prier devant ce poster coloré et apaisant\n"
      "3. Repartir immédiatement"
   );

   if (choix(L"1")) return (page)p8;
   if (choix(L"2")) return (page)p36;
   if (choix(L"3")) return (page)p25;
   return NULL;
}

page p28(void) 
{
   msg(L" == 28 ==\n"
      "Je lui parle de ma philosophie de la vie, et des buts élevés auxquels chacun devrait se référer pour se réaliser dans ce vaste univers. Je lui parle de la beauté, je lui parle de la volonté, de l’ascétisme et de l’humilité.\n\n"
      "Les yeux pleins de larmes, la femme m’embrasse, s’accrochant de façon embarrassante à mon cou, et reniant ses anciens amants.\n"
   );

   msg(L""
      "1. L’inviter dans un salon de thé, pour s’entretenir plus longuement avec elle sur des sujets essentiels\n"
      "2. Lui donner congé maintenant, pour entretenir son excitation en vue d’un rendez-vous ultérieur, et ne pas arriver trop tard chez moi"
   );

   if (choix(L"1")) return (page)p6;
   if (choix(L"2")) return (page)p38;
   return NULL;
}

page p29(void) 
{
   msg(L" == 29 ==\n"
      "Dès que j’entre dans la bâtisse cyclopéenne, je sais que cet endroit est parfait selon tous mes critères, une vie entière à passer au service de la littérature, même avec ma grammaire approximative et mon accent Francimi."
   );

/* Fin */
   return NULL;
}

page p30(void) 
{
   msg(L" == 30 ==\n"
      "Nous naviguons vers une île étrange, une longue piste de jaune ocre, avec en toile de fond des falaises vertes surplombant la plage. Une fois arrivé sur le sable, je trouve sur le sol une bouteille fermée. Il y a un vieux parchemin dedans.\n"
   );

   msg(L""
      "1. Examiner la bouteille et son contenu\n"
      "2. Explorer l’île\n"
      "3. Retourner au bateau"
   );

   if (choix(L"1")) return (page)p26;
   if (choix(L"2")) return (page)p15;
   if (choix(L"3")) return (page)p11;
   return NULL;
}

page p31(void) 
{
   msg(L" == 31 ==\n"
      "Après être entré dans une calèche tirée par des Frisons, j’atteins rapidement la petite ville où je réside habituellement, dans la banlieue de cette métropole, et je fais arrêter le véhicule au centre-ville.\n"
   );

   msg(L""
      "1. Vais-je réellement rentrer directement chez moi maintenant\n"
      "2. ou rendre visite à une amie à moi auparavant ? Je ne l’ai pas vue depuis un moment, et nous avons sans doute le droit de passer un peu de bon moment ensemble"
   );

   if (choix(L"1")) return (page)p38;
   if (choix(L"2")) return (page)p4;
   return NULL;
}


page p32(void) 
{
   msg(L" == 32 ==\n"
      "Quelqu’un m’a vendu, tandis que j’effectuais une importante mission : je devais rencontrer un dirigeant de l’Indigo Love Syndicate, pour le capturer, mais c’était sans doute un piège, et c’est moi qui ai été capturé à la place.\n"
   );

   msg(L"1. Aller en prison");

   if (choix(L"1")) return (page)p48;
   return NULL;
}

page p33(void) 
{
   msg(L" == 33 ==\n"
      "Après quelques recherches dans les bibliothèques et musées, je découvre que cette société a été fondée il y a un millier d’années, après sa séparation physique du Vieux Continent. Elle a grandi à une allure normale, sans événement marquant. Plus inquiétant est le fait que depuis une dizaine d’années, une nouvelle secte, appelée l’Indigo Love Syndicate, se répand rapidement à travers le pays, tant parmi le peuple que les élites.\n"
      "Discrète à ses débuts, la secte est maintenant un pouvoir politique sur lequel il faut compter.\n"
   );

   msg(L""
      "1. Je peux rejoindre l’opposition, pour voir s’ils sont meilleurs que ceux qu’ils prétendent combattre\n"
      "2. Je peux oublier toutes ces théories conspirationnistes, et mener une vie normale, en recherchant un petit travail paisible"
   );

   if (choix(L"1")) return (page)p9;
   if (choix(L"2")) return (page)p52;
   return NULL;
}

page p34(void) 
{
   msg(L" == 34 ==\n"
      "Lors d’une journée ensoleillée où la plupart des domestiques sont dehors avec la famille lors d’une promenade au bord de mer, j’arrive à désactiver la clôture électrique et à escalader les hauts murs entourant la propriété, pour rejoindre la liberté et l’auto-réalisation.\n\n"
      "Partant précipitamment, j’ai oublié de prendre de la nourriture et quelques affaires avec moi, aussi je me sens rapidement bien perdu dans ces rues étrangères, avec mon libre-arbitre et mes poches vides.\n"
   );

   msg(L""
      "1. Aller rencontrer les autorités locales The Local Authorities, pour m’enregistrer dans leurs services et pouvoir commencer un travail honnête et rémunéré\n"
      "2. En apprendre plus sur leurs coutumes et traditions\n"
      "3. Voler un peu de nourriture et des choses de première nécessité"
   );

   if (choix(L"1")) return (page)p47;
   if (choix(L"2")) return (page)p33;
   if (choix(L"3")) return (page)p40;
   return NULL;
}

page p35(void) 
{
   msg(L" == 35 ==\n"
      "Je la compare à une pouliche sublime et splendide, à la déesse de la fertilité, de la guerre et du désir, à l’infirmière primordiale et ainsi de suite.\n"
   );

   msg(L"1. Forte comme elle l’est, tout ce que je reçois c’est une gifle bien méritée, et avec la mâchoire et la joue aussi rouges qu’un diable excité ayant mangé du piment ; honteux, je repars vers la ville");

   if (choix(L"1")) return (page)p41;
   return NULL;
}

page p36(void) 
{
   msg(L" == 36 ==\n"
      "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\n\n"
      "Je prie avec la complicité de tous les adeptes de l’Indigo Love Syndicate.\n\n"
      "J’ai à ce moment le sentiment que mon âme vient d’être souillée.\n"
   );

   msg(L"1. Je continue néanmoins à déambuler dans les rues");

   if (choix(L"1")) return (page)p22;
   return NULL;
}

page p37(void) 
{
   msg(L" == 37 ==\n"
      "Pensant probablement que j’ai mal interprété son activité d’attente, la femme semble énervée d’avoir été dérangée par mon arrivée. Elle me dit carrément qu’elle attend quelqu’un d’autre et se moque de ma vie disgracieuse.\n"
   );

   msg(L""
      "1. Insister un peu plus\n"
      "2. L’insulter\n"
      "3. Retourner chez moi"
   );

   if (choix(L"1")) return (page)p28;
   if (choix(L"2")) return (page)p46;
   if (choix(L"3")) return (page)p31;
   return NULL;
}

page p38(void) 
{
   msg(L" == 38 ==\n"
      "Mon appartement se situe dans une maison presque déserte, elle-même sur trois niveaux. J’habite au dernier étage, et ainsi je peux admirer les bois environnants depuis ma salle de bain. Il y a une grande forêt dans la partie orientale de la ville, où les gens peuvent aller chasser, randonner et se ressourcer loin de la vie moderne et stressante.\n\n"
      "Je me sens si fatigué, que je vais au lit immédiatement.\n"
   );

   msg(L"1. Me lever au 55");

   if (choix(L"1")) return (page)p55;
   return NULL;
}

page p39(void) 
{
   msg(L" == 39 ==\n"
      "Après avoir été assommé par un pirate aux dents pourries, je suis conduit dans leur repaire, qui est caché au milieu d’une grande ville que je n’ai jamais vue auparavant. Se pourrait-il que je sois arrivé sur le Nouveau Continent en fin de compte ?\n\n"
      "Tous les marins ont été séparés pour éviter la rébellion, et je rencontre dans mon cachot des gens avec des langues et des cultures que je ne comprends pas du tout.\n"
   );

   msg(L"1. J’entends un jour que les pirates ont prévu de nous vendre en tant qu’esclaves");

   if (choix(L"1")) return (page)p16;
   return NULL;
}

page p40(void) 
{
   msg(L" == 40 ==\n"
      "Voler est facile ici : on dirait que personne n’est sur ses gardes et ainsi je peux arnaquer pas mal de monde. En fait, ce que je ne savais pas, c’était que les citoyens ne se méfiaient jamais des actes délictueux, car ici la police est tellement efficace que les criminels sont attrapés sous quarante-huit heures.\n"
   );

   msg(L"1. Aller en prison");

   if (choix(L"1")) return (page)p48;
   return NULL;
}


page p41(void) 
{
   msg(L" == 41 ==\n"
      "Accablée par la faute, cette humeur funeste embrouille mon esprit dans une dépression bien connue. Errant dans la ville sans but, je traverse un pont décoré de fer forgé. Un lampiste s’affaire à illuminer les rues, sans sourire aucun. Le crépuscule s’estompe, remplacé bientôt par la nuit noire. Le ciel indigo se mêle à la rivière indigo, et j’aspire à une telle harmonie qui me fait cruellement défaut.\n"
   );
	
   return chance((page)p12, (page)p17);
}


page p42(void) 
{
   msg(L" == 42 ==\n"
      "L’appartement de Béatrix est confortable et moderne : il y a là beaucoup de meubles en bois orné de motifs en cuivre et des tapisseries en laine décorées de fibres optiques.\n\n"
      "Elle commence à retirer ses habits, mais se ravise et me dit qu’elle préfère finalement prendre un verre à la taverne.\n"
   );

   msg(L"1. Sortir de nouveau, pour trouver un bar");

   if (choix(L"1")) return (page)p7;
   return NULL;
}


page p43(void) 
{
   msg(L" == 43 ==\n"
      "La partie occidentale de cette mer est réputée pour ses tornades fourbes et ses tourbillons imprévus.\n"
      "Le destin de notre embarcation est pourtant mis à mal par des désastres bien plus humains, lorsqu’un bateau pirate nous prend d’assaut.\n\n"
      "Ces rustres passent à l’abordage, emplis de haine et de violence, et même si nous essayons de nous rebeller, nous ne sommes pas assez fort pour contenir leurs attaques pour très longtemps.\n"
   );

   return chance((page)p39, (page)p54);
}


page p44(void) 
{
   msg(L" == 44 ==\n"
      "Ils m’accueillent avec une certaine sympathie, et prennent mon nom. Ils notent ma demande, et me donnent le numéro d’un bureau au premier étage.\n"
   );

   return chance((page)p52, (page)p20);
}


page p45(void) 
{
   msg(L" == 45 ==\n"
      "Monsieur et Madame Caesar possèdent une maison qui est plus grosse que tout ce que j’ai pu voir auparavant : un terrain d’atterrissage pour aéronef, une tour dans le jardin, et de l’électricité partout pour aider à la pousse des plantes et des légumes…\n\n"
      "Ils m’ont conduit à leur atelier, où je suis censé assembler des choses et réparer tous les gadgets qu’ils ont dans leur maison : des oiseaux mécaniques, des machines électriques, des phonographes… Après un an passé ici, ils commencent à me faire confiance.\n"
   );

   msg(L""
      "1. Vais-je essayer de m’échapper à un moment où ils me surveillent le moins\n"
      "2. ou continuer à vivre ici en tant que travailleur bon marché"
   );

   if (choix(L"1")) return (page)p34;
   if (choix(L"2")) return (page)p48;
   return NULL;
}

page p46(void) 
{
   msg(L" == 46 ==\n"
      "Sortant d’un coin sombre, un homme vêtu de noir me frappe dans le dos.\n"
   );

   msg(L"1. Je vois un voile bleu qui recouvre ma vision");

   if (choix(L"1")) return (page)p14;
   return NULL;
}

page p47(void) 
{
   msg(L" == 47 ==\n"
      "Ils m’accueillent avec beaucoup d’attention, et tandis qu’ils enregistrent mon nom, on me montre une liste des emplois possibles dans le coin.\n\n"
      "J’ai sans doute de la chance, parce qu’il vient de se libérer une place de bibliothécaire dans la bibliothèque principale de la ville.\n\n"
      "Après m’avoir fait signer quelques papiers, ils me montrent la route sur une carte.\n"
   );

   msg(L""
      "1. Aller directement là-bas, au cas où quelqu’un d’autre prendrait cette place avant moi\n"
      "2. En apprendre un peu plus sur les coutumes et traditions de la région"
   );

   if (choix(L"1")) return (page)p2;
   if (choix(L"2")) return (page)p33;
   return NULL;
}

page p48(void) 
{
   msg(L" == 48 ==\n"
      "Après quelques années en tant que prisonnier, je m’assagis et deviens plus raisonnable une fois libéré.\n\n"
      "Sans doute que le mieux pour moi maintenant est de retourner chez moi, et recommencer ma vie là-bas.\n\n"
      "Serai-je bibliothécaire, ou aventurier ? Je ne peux encore me décider."
   );

/* Fin */
   return NULL;
}

page p49(void) 
{
   msg(L" == 49 ==\n"
      "Durant une mission difficile, je sens que je n’obtiens pas l’aide que je mérite habituellement, et j’ai la nette impression de tomber dans un piège, sans moyen d’en réchapper.\n"
   );

   msg(L"1. Tomber dans le piège");

   if (choix(L"1")) return (page)p51;
   return NULL;
}

page p50(void) 
{
   msg(L" == 50 ==\n"
      "Je lui raconte ma philosophie de vie, et lui parle des buts que chacun devrait souhaiter atteindre dans l’univers. Je lui parle de la beauté, de volonté, d’ascétisme et d’humilité.\n"
   );

   msg(L"1. La jeune femme, avec pitié et un sourire perplexe, me reconduit au 44");

   if (choix(L"1")) return (page)p44;
   return NULL;
}

page p51(void) 
{
   msg(L" == 51 ==\n"
      "Un adepte Indigo Love, surgissant d’un recoin sombre, me montre son amour et son attention en me poignardant dans le dos.\n"
   );

   msg(L"1. Un voile bleu recouvre alors ma vision");

   if (choix(L"1")) return (page)p14;
   return NULL;
}

page p52(void) 
{
   msg(L" == 52 ==\n"
      "Dans ce bureau on me présente une liste des emplois possibles dans la région.\n\n"
      "J’ai sans doute de la chance, parce qu’il vient de se libérer une place de bibliothécaire dans la bibliothèque principale de la ville.\n\n"
      "Après m’avoir fait signer quelques papiers, ils m’indiquent la route sur une carte.\n"
   );

   msg(L""
      "1. Aller immédiatement là-bas, au cas où quelqu’un d’autre prendrait la place avant moi\n"
      "2. En apprendre plus sur leurs coutumes et traditions"
   );

   if (choix(L"1")) return (page)p29;
   if (choix(L"2")) return (page)p33;
   return NULL;
}

page p53(void) 
{
   msg(L" == 53 ==\n"
      "Une jeune femme polie et agréable m’explique avec une voix douce que le bureau que je recherche se trouve maintenant au rez-de-chaussée.\n"
   );

   msg(L""
      "1. Ce qui me conduit au 44\n"
      "2. mais avant cela, je peux tenter de la séduire, on ne sait jamais"
   );

   if (choix(L"1")) return (page)p44;
   if (choix(L"2")) return (page)p50;
   return NULL;
}

page p54(void) 
{
   msg(L" == 54 ==\n"
      "Pendant l’attaque, je vois le flash bleu acier de l’épée d’un pirate, approchant si dangereusement de ma tête que je finis par en mourir.\n"
   );

   msg(L"1. Aller au 14");

   if (choix(L"1")) return (page)p14;
   return NULL;
}

page p55(void) 
{
   msg(L" == 55 ==\n"
      "Baillant et m’étirant, je me réveille de bonne humeur. Je n’ai pas découvert de nouveau continent, je n’ai pas vécu de grandes aventures, mais j’ai rencontré des gens qui n’étaient pas si mal en fin de compte.\n\n"
      "Jour après jour, j’ai retrouvé mon amie Béatrix de plus en plus souvent, et elle fait de son mieux pour supporter mon humour grinçant. Elle me dit tout le temps que notre histoire est vraiment comme une fin heureuse finalement !"
   );

/* Fin */
   return NULL;
}

