/*
 * Fichier associ� et inclus par cyoa_dos.c 
 * Source encoding cp850
*/
page p0(void);
page p1(void);
page p2(void);
page p3(void);
page p4(void);
page p5(void);
page p6(void);
page p7(void);
page p8(void);
page p9(void);
page p10(void);
page p11(void);
page p12(void);
page p13(void);
page p14(void);
page p15(void);
page p16(void);
page p17(void);
page p18(void);
page p19(void);
page p20(void);
page p21(void);
page p22(void);
page p23(void);
page p24(void);
page p25(void);
page p26(void);
page p27(void);
page p28(void);
page p29(void);
page p30(void);
page p31(void);
page p32(void);
page p33(void);
page p34(void);
page p35(void);
page p36(void);
page p37(void);
page p38(void);
page p39(void);
page p40(void);
page p41(void);
page p42(void);
page p43(void);
page p44(void);
page p45(void);
page p46(void);
page p47(void);
page p48(void);
page p49(void);
page p50(void);
page p51(void);
page p52(void);
page p53(void);
page p54(void);
page p55(void);

page p0(void)
{
   msg(" La mort bleue\nUne fiction interactive d'Otto Grimwald.\n\n"
      "Pour progresser dans le jeu, choisissez parmi les diff�rentes propositions celle qui vous convient, tapez le chiffre qui la pr�c�de et validez-le en appuyant sur la touche 'Entr�e'. Vous pouvez quitter la partie en cours en tapant 'fin'.\n"
   );

   msg("1. Et maintenant, tournez la page !");

   if (choix("1")) return (page)p1;
   return NULL;
}

page p1(void) 
{
   msg(" == 1 ==\n"
      "Je me sens tomber. Pas tr�s haut par rapport au sol, heureusement pour moi. Et quelques cordages �pais amortissent ma chute. J'ai probablement encore rat� un �chelon, car je suis dans la lune la plupart du temps en ce moment.\n"
   );

   msg("1. Je me l�ve");

   if (choix("1")) return (page)p19;
   return NULL;
}

page p2(void) 
{
   msg(" == 2 ==\n"
      "D�s que je pose le pied sur le seuil de ce grand b�timent, des officiers de police m'arr�tent, au motif que je me suis �vad� de la maison o� je devais rester, ayant �t� acquit l�galement par M. et Mme Caesar.\n"
   );

   msg("1. En d�pit de mes protestations, je suis jet� en prison");

   if (choix("1")) return (page)p48;
   return NULL;
}

page p3(void) 
{
   msg(" == 3 ==\n"
      "Il y a l� un groupe de six marins. Ils ne sont qu'en train de s'amuser apr�s tout !\n"
   );

   msg(
      "1. Mais je peux leur donner une correction m�rit�e\n"
      "2. ou bien m'enfuir l�chement sur le pont sup�rieur"
   );

   if (choix("1")) return (page)p23;
   if (choix("2")) return (page)p5;
   return NULL;
}

page p4(void) 
{
   msg(" == 4 ==\n"
      "Je sonne � la porte. B�atrix m'ouvre, et me voyant, ne semble pas tr�s heureuse de ma pr�sence. Nous �tions bons amis auparavant, aussi je ne comprends pas ses r�serves.\n\n"
      "Elle porte une jolie petite robe l�g�re, blanche et grise, et je ressens tant de po�sie dans l'atmosph�re que je ne peux m'emp�cher de lui dire ce que j'ai sur le c�ur.\n"
   );

   msg(
      "1. �tre grossier, mais direct\n"
      "2. �tre mod�r�, mais verbeux"
   );

   if (choix("1")) return (page)p35;
   if (choix("2")) return (page)p28;
   return NULL;
}

page p5(void) 
{
   msg(" == 5 ==\n"
      "Ce pont est habituellement r�serv� aux passagers de premi�re classe. J'esp�re qu'aucun officier du navire ne me remarquera, sinon j'aurai de s�rieux probl�mes.\n"
      "J'obtins ce travail sur un navire pour payer ma travers�e vers le Grand Continent, qui a �t� d�couvert il y a seulement quelques si�cles.\n"
      "En attendant, j'ai toujours �t� curieux au sujet de la vie l�-bas, de savoir si les gens �taient comme nous ou si leur culture �tait si �trang�re qu'il n'�tait pas possible d'y vivre.\n\n"
      "J'ai �galement essay� d'apprendre leur si difficile langue, et je me suis forc� � l'utiliser quotidiennement. C'est ainsi que j'ai d�m�nag� vers Iricimia, qui a une langue proche, et m�me si je suis loin de compl�tement la ma�triser, j'ai v�cu l�-bas un moment, puis j'ai d�cid� d'embarquer sur ce bateau.\n"
   );

   msg(
      "1. Apr�s quelques jours, nous arrivons � New Londrin Haven, et j'entre triomphalement dans la ville\n"
      "2. mais avant cela, je peux explorer un moment le port, amoindrissant par contre le prestige de mon arriv�e"
   );

   if (choix("1")) return (page)p25;
   if (choix("2")) return (page)p27;
   return NULL;
}

page p6(void) 
{
   msg(" == 6 ==\n"
      "Nous entrons dans un salon de th� � l'ambiance snobinarde, dans une zone touristique. Nous nous ennuyons rapidement en un tel lieu, aussi je propose que nous cherchions un endroit plus agr�able pour nous divertir.\n"
   );

   msg(
      "1. Proposer son appartement\n"
      "2. Proposer mon appartement\n"
      "3. Proposer de prendre un verre dans une taverne proche"
   );

   if (choix("1")) return (page)p42;
   if (choix("2")) return (page)p38;
   if (choix("3")) return (page)p7;
   return NULL;
}

page p7(void) 
{
   msg(" == 7 ==\n"
      "Entrant dans la taverne, je retrouve certains de mes anciens camarades du bateau. �trangement, ils ne sont pas trop hostiles � ma pr�sence. Nous prenons une bi�re, puis une autre, et finalement je termine la nuit � danser sur les tables, pour la plus grande joie des femmes ici qui frappent dans leurs d�licieuses mains en rythme.\n\n"
      "J'en viens � appr�cier cette vie simple, et je ne crains plus cette terreur bleue que j'avais l'habitude de ressentir dans le pass�."
   );

/* Fin */
   return NULL;
}

page p8(void) 
{
   msg(" == 8 ==\n"
      "Ai-je de la chance aujourd'hui ? Quand je tire un d� de ma poche, je peux �tre consid�r� comme chanceux si j'obtiens un 5 ou un 6.\n"
   );

   return chance((page)p18, (page)p51);
}

page p9(void) 
{
   msg(" == 9 ==\n"
      "L'opposition est remplie de personnes charmantes et engag�es, n�anmoins je ne peux garantir que leurs dirigeants soient aussi propres politiquement. Il y en a m�me qui racontent que la t�te pensante de l'opposition est infiltr�e par l'Indigo Love Syndicate.\n"
   );

   return chance((page)p32, (page)p49);
}

page p10(void) 
{
   msg(" == 10 ==\n"
      "Les quais ne sont pas un endroit tr�s agr�able pour un �tre aussi d�licat que moi. J'ai imm�diatement le sentiment que je pourrais devenir une proie facile pour la plupart des vautours qui s�vissent ici.\n\n"
       "Je remarque une belle femme qui attend pr�s d'un ponton. Au m�me moment, des marins, rencontr�s sur le bateau o� j'�tais employ�, m'appelle tandis qu'ils entrent dans une taverne.\n"
   );

   msg(
      "1. M'approcher de cette dame\n"
      "2. Suivre mes anciens camarades dans la taverne\n"
      "3. Retourner chez moi"
   );

   if (choix("1")) return (page)p37;
   if (choix("2")) return (page)p7;
   if (choix("3")) return (page)p31;
   return NULL;
}

page p11(void) 
{
   msg(" == 11 ==\n"
      "Impassible aux �motions humaines, le navire atteint les premi�res brumes matinales de l'autre continent. Foulant la jet�e et la terre ferme pour la premi�re fois apr�s trois semaines de torture spirituelle, je me sens �tourdi et compl�tement perdu.\n\n"
      "Il y a un grand poster sur un mur, et la sortie vers le reste de la ville.\n"
   );

   msg(
      "1. Regarder le poster\n"
      "2. Sortir"
   );

   if (choix("1")) return (page)p27;
   if (choix("2")) return (page)p25;
   return NULL;
}

page p12(void) 
{
   msg(" == 12 ==\n"
      "Avant de commettre l'irr�parable, je remarque l'ombre de B�atrix arrivant derri�re moi, boulevers�e et angoiss�e � mon sujet.\n"
   );

   msg("1. Elle me dit qu'elle regrette sa r�action trop vive, et propose que l'on prenne un verre ensemble dans un salon de th� dans les environs");

   if (choix("1")) return (page)p6;
   return NULL;
}

page p13(void) 
{
   msg(" == 13 ==\n"
      "Le capitaine, arrivant peu apr�s, s'excuse pour la conduire grossi�re de ses hommes. S'inclinant profond�ment, il me demande mon opinion sur la route � suivre. Est-ce que je r�ve ? il est devenu fou, ou bien c'est une sorte de piti� malsaine � mon encontre�\n"
   );

   msg(
      "1. Au nord, toujours vers notre destination initialement pr�vue, le Nouveau Continent\n"
      "2. Au sud, pour atteindre une petite �le qu'il a juste d�couverte avec sa longue-vue\n"
      "3. � l'ouest, pour explorer encore plus ce vaste oc�an bleu\n"
      "4. � l'est, pour retourner vers notre pays d'origine"
   );

   if (choix("1")) return (page)p11;
   if (choix("2")) return (page)p30;
   if (choix("3")) return (page)p43;
   if (choix("4")) return (page)p24;
   return NULL;
}

page p14(void) 
{
   msg(" == 14 ==\n"
      "Que la mort peut �tre douce, lorsqu'elle est si bleue !"
   );

/* Fin */
   return NULL;
}

page p15(void) 
{
   msg(" == 15 ==\n"
      "Le soleil est haut dans le ciel, et pourtant je ressens une brise fra�che d�vorant mon cou et mes c�tes. Je me souviens de mon enfance en Francimia, mes jeux dans les arbres et entre les rares collines vertes de notre domaine familial. Plus tard, toujours dans les arbres, en train de lire des livres � th�me fantastique, allong� sur le tapis rouge et violet dans ma cabane � huit m�tres du sol. Je me rappelle �galement de ma m�re m'appelant, avec sa voix haut perch�e, pour venir manger la � Gargouaillotte � la Pisaille �, un plat local.\n\n"
      "J'ai soudain l'impression de tourner en rond�\n"
   );

   msg(
      "1. Continuer � explorer\n"
      "2. Retourner au bateau"
   );

   if (choix("1")) return (page)p21;
   if (choix("2")) return (page)p11;
   return NULL;
}

page p16(void) 
{
   msg(" == 16 ==\n"
      "Je n'aurais jamais imagin� qu'�tre vendu comme esclave puisse se passer ainsi. Ce n'�tait certes pas le march� aux esclaves sur la place publique comme on aurait pu l'imaginer, j'ai juste �t� conduit dans une cave sombre et humide, o� je me suis ennuy� � mourir.\n"
   );

   msg("1. Quelques jours plus tard, j'ai �t� vendu � une famille ais�e");

   if (choix("1")) return (page)p45;
   return NULL;
}

page p17(void) 
{
   msg(" == 17 ==\n"
      "Je plonge dans l'eau sombre. Je me sens si seul, que rapidement j'arr�te de respirer.\n"
   );

   msg("1. Je coule plus profond�ment");

   if (choix("1")) return (page)p14;
   return NULL;
}

page p18(void) 
{
   msg(" == 18 ==\n"
      "Les murs sont maintenant plus propres sans ce poster malsain.\n"
   );

   msg("1. Je peux continuer mon chemin");

   if (choix("1")) return (page)p25;
   return NULL;
}

page p19(void) 
{
   msg(" == 19 ==\n"
      "Je me suis senti d�prim� tant d'ann�es que je ne puis le supporter plus longtemps. De plus, mon lieu de r�sidence et mon emploi actuel menacent graduellement tant ma sant� mentale que physique�\n\n"
      "L'oc�an, immense et bleu, s'�tendant � l'infini vers le 'nord', l'est', le 'sud' et l'ouest', refl�te le soleil comme une cotte de maille scintillante.\n\n"
      "� H� ! Esp�ce de saloperie de Francimi. Viens par ici ! �, crient-ils.\n\n"
      "Entour� de ces �trangers Scottani et Iricimi, je ne me sens pas tr�s bien dans cet environnement. Leurs casques verts et orange fonc�s me rendent nerveux. Je n'ai pas besoin de leur ob�ir, car ce sont de simples marins comme moi. Mais vais-je tol�rer ces insultes ?\n"
   );

   msg(
      "1. Me rebeller, rendez-vous au 3\n"
      "2. Attendre (rendez-vous au 13)\n"
      "3. Sauter dans la mer\n"
      "4. ou explorer un peu le bateau, ce qui est un pr�texte pour fuir l�chement vers le 5"
   );

   if (choix("1")) return (page)p3;
   if (choix("2")) return (page)p13;
   if (choix("3")) return (page)p17;
   if (choix("4")) return (page)p5;
   if (choix("nord"))  return (page)p11;
   if (choix("sud"))   return (page)p30;
   if (choix("est"))   return (page)p24;
   if (choix("ouest")) return (page)p43;
   return NULL;
}

page p20(void) 
{
   msg(" == 20 ==\n"
      "On me dit avec un ton irrit� que je fais erreur. Ce n'est plus l� depuis un moment.\n"
   );

   msg("1. On me donne un autre num�ro de bureau, au cinqui�me �tage, ce qui me conduit au 53");

   if (choix("1")) return (page)p53;
   return NULL;
}

page p21(void) 
{
   msg(" == 21 ==\n"
      "� l'�ge de dix-neuf ans, je suis all� dans la ville la plus proche, pour apprendre � devenir biblioth�caire. Mon p�re en fut un peu d��u, lui qui souhaitait que je devienne artiste. J'ai quand m�me emport� mon accord�on avec moi, pour lui faire plaisir.\n\n"
      "Satan�s moustiques ! Avec toutes ces pestes volantes, il est bien difficile de se concentrer.\n"
   );

   msg(
      "1. Explorer un peu plus\n"
      "2. Retourner au bateau"
   );

   if (choix("1")) return (page)p15;
   if (choix("2")) return (page)p11;
   return NULL;
}

page p22(void) 
{
   msg(" == 22 ==\n"
      "Cette ville est �norme selon les standards du Vieux Continent. De gigantesques dirigeables traversent pompeusement les airs, mais il y a �galement quelques avions, plus qu'en Euralinia.\n\n"
      "Les maisons ici sont plus opulentes, avec des influences marqu�es des villes d'Asinalia les plus extr�mes-orientales, d�notant un go�t certain pour les d�corations d�licates et compliqu�es. Je n'aime pas cela du tout.\n\n"
      "Que puis-je faire maintenant ?\n"
   );

   msg(
      "1. Rencontrer les autorit�s locales, pour m'annoncer et m'enregistrer dans leurs services\n"
      "2. En apprendre plus sur leurs coutumes et traditions"
   );

   if (choix("1")) return (page)p44;
   if (choix("2")) return (page)p33;
   return NULL;
}

page p23(void) 
{
   msg(" == 23 ==\n"
      "Des marins entra�n�s et en nombre sont certainement plus costauds qu'un pauvre vagabond comme moi qui a toujours v�cu sur le plancher des vaches, entre la biblioth�que du coin et l'universit�.\n"
   );

   msg("1. Ils me jettent par-dessus bord");

   if (choix("1")) return (page)p17;
   return NULL;
}

page p24(void) 
{
   msg(" == 24 ==\n"
      "Le navire n'avait probablement pas assez de vivres pour effectuer la travers�e, et cela n'�tait pas une surprise de devoir faire demi-tour pour retourner dans notre pays d'origine.\n\n"
      "D��us et fatigu�s, les marins quittent le bateau en colonnes disciplin�es. Habitu�s aux petites travers�es locales, ils ne sont pas pr�s de retourner vers ce Nouveau Continent dont tout le monde parle.\n\n"
      "Je suis malgr� tout heureux de revenir ici, et d'�tre d�barrass� d'eux.\n"
   );

   msg(
      "1. Recommencer le voyage depuis le d�but\n"
      "2. Visiter les docks\n"
      "3. Retourner chez moi"
   );

   if (choix("1")) return (page)p1;
   if (choix("2")) return (page)p10;
   if (choix("3")) return (page)p31;
   return NULL;
}

page p25(void) 
{
   msg(" == 25 ==\n"
      "Cette ville est �norme selon les standards du Vieux Continent. De gigantesques dirigeables traversent pompeusement les airs, mais il y a �galement quelques avions, plus qu'en Euralinia.\n\n"
      "Les maisons ici sont plus opulentes, avec des influences marqu�es des villes d'Asinalia les plus extr�mes-orientales, d�notant un go�t certain pour les d�corations d�licates et ayant du caract�re.\n\n"
      "Une telle sollicitude pour des motifs aussi inspir�s m'inspire un tel bonheur que j'en oublie un moment ma solitude spirituelle.\n\n"
      "Le soleil commence � annihiler l'humidit� poisseuse de la brume, en envoyant des rayons orange sur les murs.\n\n"
      "Que faire maintenant ?\n"
   );

   msg(
      "1. Rencontrer les autorit�s locales, pour m'annoncer et m'enregistrer dans leurs services\n"
      "2. En apprendre plus sur leurs coutumes et traditions\n"
      "3. Voler un peu de nourriture, personne ne me conna�t ici apr�s tout"
   );

   if (choix("1")) return (page)p44;
   if (choix("2")) return (page)p33;
   if (choix("3")) return (page)p40;
   return NULL;
}

page p26(void) 
{
   msg(" == 26 ==\n"
      "� Cher voyageur,\n"
      "merci de participer � ce jeu. J'esp�re que vous ne vous sentirez pas trop limit� ici, et que votre libert� de pens�e n'est pas trop mise � mal. Cette courte histoire risque par contre de r�ellement commencer quand vous retournerez � la civilisation. N'esp�rez pas d�couvrir beaucoup de choses dans ces �les d�sert�es. Au fait, si par hasard vous rencontrez Jacqueline, dites-lui bonjour de notre part ! �\n"
   );

   msg("1. L'esprit en paix et oubliant la tromperie verdoyante de la montagne, je manie fr�n�tiquement les rames pour retourner au bateau");

   if (choix("1")) return (page)p11;
   return NULL;
}

page p27(void) 
{
   msg(" == 27 ==\n"
      "Ce qui �tait le plus visible et �trange dans cet endroit, c'�tait un poster imprim� sur un fond bleu indigo, avec �crit par-dessus, en lettres �normes et roses :\n\n"
      "� REJOIGNEZ L'ARM�E DE L'AMOUR !\n"
      "L'Indigo Love Syndicate a besoin de vous. Merci pour votre attention. �\n\n"
      "Il n'y a pas de moyen clair pour les contacter. Peut-�tre qu'ils sont devenus si puissants r�cemment qu'il est ais� de tomber sur eux. On verra bien !\n"
   );

   msg(
      "1. D�chirer ce poster\n"
      "2. S'agenouiller et prier devant ce poster color� et apaisant\n"
      "3. Repartir imm�diatement"
   );

   if (choix("1")) return (page)p8;
   if (choix("2")) return (page)p36;
   if (choix("3")) return (page)p25;
   return NULL;
}

page p28(void) 
{
   msg(" == 28 ==\n"
      "Je lui parle de ma philosophie de la vie, et des buts �lev�s auxquels chacun devrait se r�f�rer pour se r�aliser dans ce vaste univers. Je lui parle de la beaut�, je lui parle de la volont�, de l'asc�tisme et de l'humilit�.\n\n"
      "Les yeux pleins de larmes, la femme m'embrasse, s'accrochant de fa�on embarrassante � mon cou, et reniant ses anciens amants.\n"
   );

   msg(
      "1. L'inviter dans un salon de th�, pour s'entretenir plus longuement avec elle sur des sujets essentiels\n"
      "2. Lui donner cong� maintenant, pour entretenir son excitation en vue d'un rendez-vous ult�rieur, et ne pas arriver trop tard chez moi"
   );

   if (choix("1")) return (page)p6;
   if (choix("2")) return (page)p38;
   return NULL;
}

page p29(void) 
{
   msg(" == 29 ==\n"
      "D�s que j'entre dans la b�tisse cyclop�enne, je sais que cet endroit est parfait selon tous mes crit�res, une vie enti�re � passer au service de la litt�rature, m�me avec ma grammaire approximative et mon accent Francimi."
   );

/* Fin */
   return NULL;
}

page p30(void) 
{
   msg(" == 30 ==\n"
      "Nous naviguons vers une �le �trange, une longue piste de jaune ocre, avec en toile de fond des falaises vertes surplombant la plage. Une fois arriv� sur le sable, je trouve sur le sol une bouteille ferm�e. Il y a un vieux parchemin dedans.\n"
   );

   msg(
      "1. Examiner la bouteille et son contenu\n"
      "2. Explorer l'�le\n"
      "3. Retourner au bateau"
   );

   if (choix("1")) return (page)p26;
   if (choix("2")) return (page)p15;
   if (choix("3")) return (page)p11;
   return NULL;
}

page p31(void) 
{
   msg(" == 31 ==\n"
      "Apr�s �tre entr� dans une cal�che tir�e par des Frisons, j'atteins rapidement la petite ville o� je r�side habituellement, dans la banlieue de cette m�tropole, et je fais arr�ter le v�hicule au centre-ville.\n"
   );

   msg(
      "1. Vais-je r�ellement rentrer directement chez moi maintenant\n"
      "2. ou rendre visite � une amie � moi auparavant ? Je ne l'ai pas vue depuis un moment, et nous avons sans doute le droit de passer un peu de bon moment ensemble"
   );

   if (choix("1")) return (page)p38;
   if (choix("2")) return (page)p4;
   return NULL;
}


page p32(void) 
{
   msg(" == 32 ==\n"
      "Quelqu'un m'a vendu, tandis que j'effectuais une importante mission : je devais rencontrer un dirigeant de l'Indigo Love Syndicate, pour le capturer, mais c'�tait sans doute un pi�ge, et c'est moi qui ai �t� captur� � la place.\n"
   );

   msg("1. Aller en prison");

   if (choix("1")) return (page)p48;
   return NULL;
}

page p33(void) 
{
   msg(" == 33 ==\n"
      "Apr�s quelques recherches dans les biblioth�ques et mus�es, je d�couvre que cette soci�t� a �t� fond�e il y a un millier d'ann�es, apr�s sa s�paration physique du Vieux Continent. Elle a grandi � une allure normale, sans �v�nement marquant. Plus inqui�tant est le fait que depuis une dizaine d'ann�es, une nouvelle secte, appel�e l'Indigo Love Syndicate, se r�pand rapidement � travers le pays, tant parmi le peuple que les �lites.\n"
      "Discr�te � ses d�buts, la secte est maintenant un pouvoir politique sur lequel il faut compter.\n"
   );

   msg(
      "1. Je peux rejoindre l'opposition, pour voir s'ils sont meilleurs que ceux qu'ils pr�tendent combattre\n"
      "2. Je peux oublier toutes ces th�ories conspirationnistes, et mener une vie normale, en recherchant un petit travail paisible"
   );

   if (choix("1")) return (page)p9;
   if (choix("2")) return (page)p52;
   return NULL;
}

page p34(void) 
{
   msg(" == 34 ==\n"
      "Lors d'une journ�e ensoleill�e o� la plupart des domestiques sont dehors avec la famille lors d'une promenade au bord de mer, j'arrive � d�sactiver la cl�ture �lectrique et � escalader les hauts murs entourant la propri�t�, pour rejoindre la libert� et l'auto-r�alisation.\n\n"
      "Partant pr�cipitamment, j'ai oubli� de prendre de la nourriture et quelques affaires avec moi, aussi je me sens rapidement bien perdu dans ces rues �trang�res, avec mon libre-arbitre et mes poches vides.\n"
   );

   msg(
      "1. Aller rencontrer les autorit�s locales The Local Authorities, pour m'enregistrer dans leurs services et pouvoir commencer un travail honn�te et r�mun�r�\n"
      "2. En apprendre plus sur leurs coutumes et traditions\n"
      "3. Voler un peu de nourriture et des choses de premi�re n�cessit�"
   );

   if (choix("1")) return (page)p47;
   if (choix("2")) return (page)p33;
   if (choix("3")) return (page)p40;
   return NULL;
}

page p35(void) 
{
   msg(" == 35 ==\n"
      "Je la compare � une pouliche sublime et splendide, � la d�esse de la fertilit�, de la guerre et du d�sir, � l'infirmi�re primordiale et ainsi de suite.\n"
   );

   msg("1. Forte comme elle l'est, tout ce que je re�ois c'est une gifle bien m�rit�e, et avec la m�choire et la joue aussi rouges qu'un diable excit� ayant mang� du piment ; honteux, je repars vers la ville");

   if (choix("1")) return (page)p41;
   return NULL;
}

page p36(void) 
{
   msg(" == 36 ==\n"
      "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\n\n"
      "Je prie avec la complicit� de tous les adeptes de l'Indigo Love Syndicate.\n\n"
      "J'ai � ce moment le sentiment que mon �me vient d'�tre souill�e.\n"
   );

   msg("1. Je continue n�anmoins � d�ambuler dans les rues");

   if (choix("1")) return (page)p22;
   return NULL;
}

page p37(void) 
{
   msg(" == 37 ==\n"
      "Pensant probablement que j'ai mal interpr�t� son activit� d'attente, la femme semble �nerv�e d'avoir �t� d�rang�e par mon arriv�e. Elle me dit carr�ment qu'elle attend quelqu'un d'autre et se moque de ma vie disgracieuse.\n"
   );

   msg(
      "1. Insister un peu plus\n"
      "2. L'insulter\n"
      "3. Retourner chez moi"
   );

   if (choix("1")) return (page)p28;
   if (choix("2")) return (page)p46;
   if (choix("3")) return (page)p31;
   return NULL;
}

page p38(void) 
{
   msg(" == 38 ==\n"
      "Mon appartement se situe dans une maison presque d�serte, elle-m�me sur trois niveaux. J'habite au dernier �tage, et ainsi je peux admirer les bois environnants depuis ma salle de bain. Il y a une grande for�t dans la partie orientale de la ville, o� les gens peuvent aller chasser, randonner et se ressourcer loin de la vie moderne et stressante.\n\n"
      "Je me sens si fatigu�, que je vais au lit imm�diatement.\n"
   );

   msg("1. Me lever au 55");

   if (choix("1")) return (page)p55;
   return NULL;
}

page p39(void) 
{
   msg(" == 39 ==\n"
      "Apr�s avoir �t� assomm� par un pirate aux dents pourries, je suis conduit dans leur repaire, qui est cach� au milieu d'une grande ville que je n'ai jamais vue auparavant. Se pourrait-il que je sois arriv� sur le Nouveau Continent en fin de compte ?\n\n"
      "Tous les marins ont �t� s�par�s pour �viter la r�bellion, et je rencontre dans mon cachot des gens avec des langues et des cultures que je ne comprends pas du tout.\n"
   );

   msg("1. J'entends un jour que les pirates ont pr�vu de nous vendre en tant qu'esclaves");

   if (choix("1")) return (page)p16;
   return NULL;
}

page p40(void) 
{
   msg(" == 40 ==\n"
      "Voler est facile ici : on dirait que personne n'est sur ses gardes et ainsi je peux arnaquer pas mal de monde. En fait, ce que je ne savais pas, c'�tait que les citoyens ne se m�fiaient jamais des actes d�lictueux, car ici la police est tellement efficace que les criminels sont attrap�s sous quarante-huit heures.\n"
   );

   msg("1. Aller en prison");

   if (choix("1")) return (page)p48;
   return NULL;
}


page p41(void) 
{
   msg(" == 41 ==\n"
      "Accabl�e par la faute, cette humeur funeste embrouille mon esprit dans une d�pression bien connue. Errant dans la ville sans but, je traverse un pont d�cor� de fer forg�. Un lampiste s'affaire � illuminer les rues, sans sourire aucun. Le cr�puscule s'estompe, remplac� bient�t par la nuit noire. Le ciel indigo se m�le � la rivi�re indigo, et j'aspire � une telle harmonie qui me fait cruellement d�faut.\n"
   );
	
   return chance((page)p12, (page)p17);
}


page p42(void) 
{
   msg(" == 42 ==\n"
      "L'appartement de B�atrix est confortable et moderne : il y a l� beaucoup de meubles en bois orn� de motifs en cuivre et des tapisseries en laine d�cor�es de fibres optiques.\n\n"
      "Elle commence � retirer ses habits, mais se ravise et me dit qu'elle pr�f�re finalement prendre un verre � la taverne.\n"
   );

   msg("1. Sortir de nouveau, pour trouver un bar");

   if (choix("1")) return (page)p7;
   return NULL;
}


page p43(void) 
{
   msg(" == 43 ==\n"
      "La partie occidentale de cette mer est r�put�e pour ses tornades fourbes et ses tourbillons impr�vus.\n"
      "Le destin de notre embarcation est pourtant mis � mal par des d�sastres bien plus humains, lorsqu'un bateau pirate nous prend d'assaut.\n\n"
      "Ces rustres passent � l'abordage, emplis de haine et de violence, et m�me si nous essayons de nous rebeller, nous ne sommes pas assez fort pour contenir leurs attaques pour tr�s longtemps.\n"
   );

   return chance((page)p39, (page)p54);
}


page p44(void) 
{
   msg(" == 44 ==\n"
      "Ils m'accueillent avec une certaine sympathie, et prennent mon nom. Ils notent ma demande, et me donnent le num�ro d'un bureau au premier �tage.\n"
   );

   return chance((page)p52, (page)p20);
}


page p45(void) 
{
   msg(" == 45 ==\n"
      "Monsieur et Madame Caesar poss�dent une maison qui est plus grosse que tout ce que j'ai pu voir auparavant : un terrain d'atterrissage pour a�ronef, une tour dans le jardin, et de l'�lectricit� partout pour aider � la pousse des plantes et des l�gumes�\n\n"
      "Ils m'ont conduit � leur atelier, o� je suis cens� assembler des choses et r�parer tous les gadgets qu'ils ont dans leur maison : des oiseaux m�caniques, des machines �lectriques, des phonographes� Apr�s un an pass� ici, ils commencent � me faire confiance.\n"
   );

   msg(
      "1. Vais-je essayer de m'�chapper � un moment o� ils me surveillent le moins\n"
      "2. ou continuer � vivre ici en tant que travailleur bon march�"
   );

   if (choix("1")) return (page)p34;
   if (choix("2")) return (page)p48;
   return NULL;
}

page p46(void) 
{
   msg(" == 46 ==\n"
      "Sortant d'un coin sombre, un homme v�tu de noir me frappe dans le dos.\n"
   );

   msg("1. Je vois un voile bleu qui recouvre ma vision");

   if (choix("1")) return (page)p14;
   return NULL;
}

page p47(void) 
{
   msg(" == 47 ==\n"
      "Ils m'accueillent avec beaucoup d'attention, et tandis qu'ils enregistrent mon nom, on me montre une liste des emplois possibles dans le coin.\n\n"
      "J'ai sans doute de la chance, parce qu'il vient de se lib�rer une place de biblioth�caire dans la biblioth�que principale de la ville.\n\n"
      "Apr�s m'avoir fait signer quelques papiers, ils me montrent la route sur une carte.\n"
   );

   msg(
      "1. Aller directement l�-bas, au cas o� quelqu'un d'autre prendrait cette place avant moi\n"
      "2. En apprendre un peu plus sur les coutumes et traditions de la r�gion"
   );

   if (choix("1")) return (page)p2;
   if (choix("2")) return (page)p33;
   return NULL;
}

page p48(void) 
{
   msg(" == 48 ==\n"
      "Apr�s quelques ann�es en tant que prisonnier, je m'assagis et deviens plus raisonnable une fois lib�r�.\n\n"
      "Sans doute que le mieux pour moi maintenant est de retourner chez moi, et recommencer ma vie l�-bas.\n\n"
      "Serai-je biblioth�caire, ou aventurier ? Je ne peux encore me d�cider."
   );

/* Fin */
   return NULL;
}

page p49(void) 
{
   msg(" == 49 ==\n"
      "Durant une mission difficile, je sens que je n'obtiens pas l'aide que je m�rite habituellement, et j'ai la nette impression de tomber dans un pi�ge, sans moyen d'en r�chapper.\n"
   );

   msg("1. Tomber dans le pi�ge");

   if (choix("1")) return (page)p51;
   return NULL;
}

page p50(void) 
{
   msg(" == 50 ==\n"
      "Je lui raconte ma philosophie de vie, et lui parle des buts que chacun devrait souhaiter atteindre dans l'univers. Je lui parle de la beaut�, de volont�, d'asc�tisme et d'humilit�.\n"
   );

   msg("1. La jeune femme, avec piti� et un sourire perplexe, me reconduit au 44");

   if (choix("1")) return (page)p44;
   return NULL;
}

page p51(void) 
{
   msg(" == 51 ==\n"
      "Un adepte Indigo Love, surgissant d'un recoin sombre, me montre son amour et son attention en me poignardant dans le dos.\n"
   );

   msg("1. Un voile bleu recouvre alors ma vision");

   if (choix("1")) return (page)p14;
   return NULL;
}

page p52(void) 
{
   msg(" == 52 ==\n"
      "Dans ce bureau on me pr�sente une liste des emplois possibles dans la r�gion.\n\n"
      "J'ai sans doute de la chance, parce qu'il vient de se lib�rer une place de biblioth�caire dans la biblioth�que principale de la ville.\n\n"
      "Apr�s m'avoir fait signer quelques papiers, ils m'indiquent la route sur une carte.\n"
   );

   msg(
      "1. Aller imm�diatement l�-bas, au cas o� quelqu'un d'autre prendrait la place avant moi\n"
      "2. En apprendre plus sur leurs coutumes et traditions"
   );

   if (choix("1")) return (page)p29;
   if (choix("2")) return (page)p33;
   return NULL;
}

page p53(void) 
{
   msg(" == 53 ==\n"
      "Une jeune femme polie et agr�able m'explique avec une voix douce que le bureau que je recherche se trouve maintenant au rez-de-chauss�e.\n"
   );

   msg(
      "1. Ce qui me conduit au 44\n"
      "2. mais avant cela, je peux tenter de la s�duire, on ne sait jamais"
   );

   if (choix("1")) return (page)p44;
   if (choix("2")) return (page)p50;
   return NULL;
}

page p54(void) 
{
   msg(" == 54 ==\n"
      "Pendant l'attaque, je vois le flash bleu acier de l'�p�e d'un pirate, approchant si dangereusement de ma t�te que je finis par en mourir.\n"
   );

   msg("1. Aller au 14");

   if (choix("1")) return (page)p14;
   return NULL;
}

page p55(void) 
{
   msg(" == 55 ==\n"
      "Baillant et m'�tirant, je me r�veille de bonne humeur. Je n'ai pas d�couvert de nouveau continent, je n'ai pas v�cu de grandes aventures, mais j'ai rencontr� des gens qui n'�taient pas si mal en fin de compte.\n\n"
      "Jour apr�s jour, j'ai retrouv� mon amie B�atrix de plus en plus souvent, et elle fait de son mieux pour supporter mon humour grin�ant. Elle me dit tout le temps que notre histoire est vraiment comme une fin heureuse finalement !"
   );

/* Fin */
   return NULL;
}

