/* Copyright (c) 2020 Auraes, pour le code en langage c
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Ce programme est un logiciel libre ; vous pouvez le redistribuer ou le 
 * modifier suivant les termes de la GNU General Public License telle que 
 * publi�e par la Free Software Foundation ; soit la version 2 de la licence, 
 * soit (� votre gr�) toute version ult�rieure.
 * Ce programme est distribu� dans 'espoir qu'il sera utile, mais SANS AUCUNE 
 * GARANTIE ; sans m�me la garantie tacite de QUALIT� MARCHANDE ou d'AD�QUATION 
 * � UN BUT PARTICULIER. Consultez la GNU General Public License pour plus de 
 * d�tails. Vous devez avoir re�u une copie de la GNU General Public License
 * en m�me temps que ce programme ; si ce n'est pas le cas, 
 * consultez <http://www.gnu.org/licenses>.

 * La Mort Bleue, copyright (c) Eric Forgeot pour le texte
 * LICENCE LIBRE : CC-BY-SA https://creativecommons.org/licenses/by-sa/3.0/deed.fr
*/

/*
   Encodage des caract�res cp850_DOS_Latin1 
   tcc -w -a -K -ms -d -1  %1
   Turbo C++ Version 3.00 Copyright (c) 1992 Borland International
*/

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <time.h>

#define SAISIEMAX 7

typedef void (*page)(void);

char buffer[SAISIEMAX+1];
int dice[] = { 1, 2, 3, 4, 5, 6 };

const unsigned char font1[] = {
/* � => oe */
   0x00, /* 00000000 */
   0x00, /* 00000000 */
   0x00, /* 00000000 */
   0x00, /* 00000000 */
   0x00, /* 00000000 */
   0x00, /* 00000000 */
   0x6c, /* 01101100 */
   0xda, /* 11011010 */
   0xda, /* 11011010 */
   0xde, /* 11011110 */
   0xd8, /* 11011000 */
   0xd8, /* 11011000 */
   0x6e, /* 01101110 */
   0x00, /* 00000000 */
   0x00, /* 00000000 */
   0x00  /* 00000000 */
};
const unsigned char font2[] = {
/* � => ... */
   0x00, /* 00000000 */
   0x00, /* 00000000 */
   0x00, /* 00000000 */
   0x00, /* 00000000 */
   0x00, /* 00000000 */
   0x00, /* 00000000 */
   0x00, /* 00000000 */
   0x00, /* 00000000 */
   0x00, /* 00000000 */
   0x00, /* 00000000 */
   0x00, /* 00000000 */
   0xdb, /* 11011011 */
   0xdb, /* 11011011 */
   0x00, /* 00000000 */
   0x00, /* 00000000 */
   0x00  /* 00000000 */
};

void msg(const char *str);
void readline(char *buf, size_t size);
int choix(const char *str);
int str_len(const char *pt);
void new_line(void);
page chance(page lucky, page unlucky);

int flag_readline;
int xc;
int cwidth;

#include "pages.c"

page chance(page lucky, page unlucky)
{
   int n, num, pt;

   n = 6;
   while(n) {
      num = rand() % n; /* 0-5 */
      n--;
      pt = dice[num];
      dice[num] = dice[n];
      dice[n] = pt;
   }
   msg("Tentez votre Chance, choisissez une des six faces du d� !");

   readline(buffer, SAISIEMAX+1);

   if (*buffer >= '1' && *buffer <= '6') {
     if (dice[*buffer - '0'] >= 5) {
         msg(" Je suis chanceux.");
         putchar('\n');
         return ((page)lucky);
     }
   }
   msg(" Je suis malchanceux.");
   putchar('\n');
   return ((page)unlucky);
}

void new_line(void)
{
   xc = 1;
   putchar('\n');
   putchar(' ');
}

int str_len(const char *pt)
{
   const char *p = pt;
   if (*pt == ' ' || *pt == '\n') return -1;
   while (*pt != '\0' && *pt != ' ' && *pt != '\n') pt++;
   return (pt-p);
}

void msg(const char *str)
{
   int i;

   while(1) {   
      i = str_len(str);
      if (i == 0) {
         new_line();
         break;
      }
      if (i == -1) {
         if (*str == '\n') new_line();
         else {
            if (xc > cwidth) {
               new_line();
               if (*str == ' ') goto b0;
            }
            putchar(*str);
            if (xc > (cwidth-1)) new_line();
            else xc++;
         }
         b0:
         str++;
         continue;
      }
      if ((xc+i) > (cwidth+1)) new_line();
      while (i) {
         putchar(*str++);
         xc++;
         i--;
      }
   }
}

int choix(const char* str)
{
   if (flag_readline == 0) {
      flag_readline = 1;
      readline(buffer, SAISIEMAX+1);
   }
   return !(strcmp(str, buffer));
}

void readline(char *buf, size_t size)
{
   int c;

   putchar('>');
   if (fgets(buf, size, stdin) == NULL) return; /* TODO */
   putchar('\n');
   while (*buf != '\0') {
      if (*buf == '\n') {
         *buf = '\0';
         return;
      }
      *buf = tolower(*buf);
      buf++;	
   }	
   do {
      c = fgetc(stdin);
   } while (c != '\n' && c != EOF); /* EOF TODO */
}

int main(int nbarg, char *argv[])
{	
   page (*pf)(void);
   page (*pt)(void);

   system("cls");
   srand((unsigned)time(NULL));

   cwidth = 66; 
   if (nbarg > 1) {
      cwidth = atoi(argv[1]);
      if (cwidth == 0 || cwidth < 40 || cwidth > 80) cwidth = 66;
   }

   asm {
      mov ax,ds
      mov es,ax
      push bp
      mov bp, offset font1
      mov ax,1110h
      mov bh,16
      mov bl,0
      mov cx,1
      mov dx,146
      int 10h
      mov bp, offset font2
      mov dx,170
      int 10h
      pop bp 
   }  

   putchar('\n');
   pf = p0;
   while(1) {
      flag_readline = 0;
      pt = (page (*)()) pf();
      if (pt == NULL) {
         if (flag_readline == 0) { /* WARNING 1er */
            msg("\nFin.\n");
            break;
         }
         if (choix("fin")) break;
      }
      else pf = pt;
   }
   return EXIT_SUCCESS;
}

